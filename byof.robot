*** Settings ***
Library    SeleniumLibrary

*** Variables ***
${URL}    https://www.saucedemo.com/
${STANDARD_USER}    standard_user
${LOCKEDOUT_USER}    locked_out_user
${PROBLEM_USER}    problem_user
${PERFORMANCEGLITCH_USER}    performance_glitch_user
${PASSWORD}    secret_sauce
${BROWSER}    headlesschrome


*** Test Cases ***
Open saucedemo.com and login as standard_user
    Open Browser    https://www.saucedemo.com    browser=${BROWSER}
    Title Should Be    Swag Labs
    Input Text    class:form_input#user-name    ${STANDARDUSER}
    Input Password    class:form_input#password    ${PASSWORD}
    Click Button    login-button
    Element Text Should Be    class:title    Products    message=Found Products title, login succesfull    ignore_case=Yes
    Close Browser

Open saucedemo.com and login as locked_out_user
    Open Browser    https://www.saucedemo.com    browser=${BROWSER}
    Title Should Be    Swag Labs
    Input Text    class:form_input#user-name    ${LOCKEDOUT_USER}
    Input Password    class:form_input#password    ${PASSWORD}
    Click Button    login-button
    Element Text Should Be    class:error-message-container    Epic sadface: Sorry, this user has been locked out.    message=Success, locked out user couldn't log in    ignore_case=Yes
    Close Browser

Open saucedemo.com and login as performance_glitch_user
    Open Browser    https://www.saucedemo.com    browser=${BROWSER}
    Title Should Be    Swag Labs
    Input Text    class:form_input#user-name    ${PERFORMANCEGLITCH_USER}
    Input Password    class:form_input#password    ${PASSWORD}
    Click Button    login-button
    Element Text Should Be    class:title    Products    message=Found Products title, login succesfull    ignore_case=Yes
    Close Browser

*** Keywords ***
    #force commit
    #and another commit
    #and another change to force a push